<?php
include 'top.php';
$query = '';
$values = '';
if (isset($_GET['btnSubmit'])) {
    if ($_GET['txtQuery'] != '') {
        $query = $_GET['txtQuery'];
    }
    if ($_GET['txtValues'] != '') {
        $values = explode(",", $_GET['txtValues']);
    }
    $thisDatabaseReader->what($query, $values);
}
?> 
<form method="get" action ="#">
    <p>
        <label for="txtQuery">Enter your query leaving in the ? marks if any</label>
        <textarea name="txtQuery" id="txtQuery" style="width: 90%; height: 10em;"><?php
            print trim($query);
            ?></textarea>     
    </p>
    <p>
        <label for = "txtValues">Enter your array values comma separated</label>
        <textarea name = "txtValues" id = "txtValues" style="width: 90%; height: 5em;"><?php
            $str = '';

            foreach ($values as $value) {
                $str .= $value . ', ';
            }

            $str = rtrim($str, ", ");
            print trim($str);
            ?></textarea>
    </p>
    <p>
        <input type = "submit" value = "Submit" name = "btnSubmit">
    </p> 
</form> 

<?php include 'footer.php'; ?>